﻿using System;

namespace TDK.Tools
{
    public static class DateTimeExtension
    {
        #region Public Methods

        public static bool MyEqual(this DateTime value, DateTime other)
        {
            if (value.Year != other.Year)
                return false;
            if (value.Month != other.Month)
                return false;
            if (value.Day != other.Day)
                return false;
            if (value.Hour != other.Hour)
                return false;
            if (value.Minute != other.Minute)
                return false;
            if (value.Second != other.Second)
                return false;

            return true;
        }

        public static string ToMySqlString(this DateTime value)
        {
            //MySql DateTime format: YYYY-MM-DD HH:MI:SS
            return value.ToString("yyyy-MM-dd HH:mm:ss");
        }

        #endregion Public Methods
    }
}