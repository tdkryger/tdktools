﻿namespace TDK.Tools
{
    public static class BoolExtension
    {
        #region Public Methods

        public static string ToIntString(this bool value)
        {
            if (value)
                return "1";
            else
                return "0";
        }

        #endregion Public Methods
    }
}