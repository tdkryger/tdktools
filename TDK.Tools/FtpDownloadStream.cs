﻿using System;
using System.IO;
using System.Net;

namespace TDK.Tools
{
    public class FtpDownloadStream : Stream
    {
        private readonly string _serverUri;
        private readonly string _userName;
        private readonly string _password;

        private FtpWebRequest _request;
        private FtpWebResponse _response;
        private Stream _responseStream;
        private int _totalDone;

        #region Properties

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return false; }
        }

        public override bool CanWrite
        {
            get { return false; }
        }

        public override long Length
        {
            get { throw new NotImplementedException(); }
        }

        public override long Position
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public Exception CaughtException;

        #endregion Properties

        #region Constructor

        public FtpDownloadStream(string serverUri, string userName, string password)
        {
            _serverUri = serverUri;
            _userName = userName;
            _password = password;
        }

        #endregion Constructor

        #region Private methods

        private void StartFtpDownload()
        {
            // This can be replaced with the Http equivalents
            _request = (FtpWebRequest)WebRequest.Create(_serverUri);
            _request.Method = WebRequestMethods.Ftp.DownloadFile;
            _request.Credentials = new NetworkCredential(_userName, _password);
            _request.ContentOffset = _totalDone;        // for resume on failure

            _response = (FtpWebResponse)_request.GetResponse();
            _responseStream = _response.GetResponseStream();
        }

        #endregion Private methods

        #region Public methods

        public override int Read(byte[] buffer, int offset, int count)
        {
            var attempts = 0;
            while (attempts++ < 5)
            {   // Adjust the maximum attempts according to your needs
                if (_responseStream == null)
                {
                    StartFtpDownload();
                }
                try
                {
                    // This will throw a timeout exception if the connection is interrupted.
                    // Will throw null exception if failed to open (start); this will also retry.
                    var done = _responseStream.Read(buffer, offset, count);

                    _totalDone += done;
                    return done;
                }
                catch (Exception ex)
                {
                    CaughtException = ex;
                    // Close ftp resources if possible. Set instances to null to force restart.
                    Close();
                }
            }
            return 0;
        }

        public override void Close()
        {
            if (_responseStream != null)
            {
                try
                {
                    _responseStream.Close();
                    _response.Close();
                }
#pragma warning disable CC0004 // Catch block cannot be empty
                catch
                {
                    // No action required
                }
#pragma warning restore CC0004 // Catch block cannot be empty
            }
            _responseStream = null;
            _response = null;
            _request = null;
        }

        #region Implement the Stream methods

        public override void Flush()
        {
            throw new NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        #endregion Implement the Stream methods

        #endregion Public methods
    }
}