﻿using System;
using System.Text;

namespace TDK.Tools
{
    public static class StringTools
    {
        #region Public methods

        public static string RandomStringOfWords(int wordCount)
        {
            Random random = new Random(DateTime.Now.Second);
            var sb = new StringBuilder();
            for (int i = 0; i < wordCount; i++)
            {
                sb.Append(WordFinder(random.Next(1, 15)));
                sb.Append(" ");
            }
            return sb.ToString().Trim();
        }

        public static string WordFinder(int requestedLength)
        {
            Random rnd = new Random();
            string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z" };
            string[] vowels = { "a", "e", "i", "o", "u" };

            string word = "";

            if (requestedLength == 1)
            {
                word = GetRandomLetter(rnd, vowels);
            }
            else
            {
                for (int i = 0; i < requestedLength; i += 2)
                {
                    word += GetRandomLetter(rnd, consonants) + GetRandomLetter(rnd, vowels);
                }
                // We may generate a string longer than requested length, but it doesn't matter if cut off the excess.
                word = word.Replace("q", "qu").Substring(0, requestedLength);
            }

            return word;
        }

        #endregion Public methods

        #region Private methods

        private static string GetRandomLetter(Random rnd, string[] letters)
        {
            return letters[rnd.Next(0, letters.Length - 1)];
        }

        #endregion Private methods
    }
}