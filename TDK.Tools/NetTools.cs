﻿using System.Net;

namespace TDK.Tools
{
    public static class NetTools
    {
        #region Public mehtods

        public static void DownloadFile(string url, string fileName)
        {
            using (WebClient client = new WebClient())
            {
                client.DownloadFile(url, fileName);
            }
        }

        #endregion Public mehtods
    }
}