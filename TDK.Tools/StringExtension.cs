﻿using HtmlAgilityPack;
using System;

namespace TDK.Tools
{
    public static class StringExtension
    {
        #region Public mehtods

        public static void CheckForNullOrEmpty(this string value)
        {
            if (value == string.Empty)
                throw new ArgumentException($"string is empty");
            if (value == null)
                throw new ArgumentNullException($"string is null");
        }

        public static string ShortHtmlString(this string text, int count)
        {
            if (text == null)
                throw new ArgumentNullException(nameof(text), "StringExtension.ShortHtmlString: Text is null");
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(text);

            // "//img" is a xpath which means select img nodes that are anywhere in the html
            foreach (var item in doc.DocumentNode.SelectNodes("//img"))
            {
                item.Remove();
            }
            var htmlString = doc.DocumentNode.OuterHtml;

            var idx = htmlString.IndexOf("</", count);
            var idx2 = htmlString.IndexOf(">", idx);

            var tempString = htmlString.Substring(0, idx2 + 1);
            //TODO: Check for unclosed tags

            return tempString;
        }

        public static string ShortString(this string text, int count)
        {
            if (text == null)
                throw new ArgumentNullException(nameof(text), "StringExtension.ShortString: Text is null");
            var temp = text;
            if (count < text.Length)
            {
                temp = text.Substring(0, count);
                var idx = temp.LastIndexOf(' ');
                temp = temp.Substring(0, idx).Trim() + "...";
            }

            return temp;
        }

        #endregion Public mehtods
    }
}