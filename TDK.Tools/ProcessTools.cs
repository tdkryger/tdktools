﻿using System;
using System.Diagnostics;
using System.Linq;

namespace TDK.Tools
{
    public static class ProcessTools
    {
        #region Public mehtods

        public static bool IsProcessRunning(string processName)
        {
            return Process.GetProcessesByName(processName).Count() > 1;
        }

        public static void LaunchApp(string pathAndFilename)
        {
            var startInfo = new ProcessStartInfo
            {
                CreateNoWindow = false,
                UseShellExecute = false,
                FileName = pathAndFilename,
                WindowStyle = ProcessWindowStyle.Normal,
            };

            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit();
                }
            }
            catch (Exception ex)
            {
                throw new ProcessException(pathAndFilename, ex.Message, ex);
            }
        }

        #endregion Public mehtods
    }

    public class ProcessException : Exception
    {
        #region Properties

        public string PathAndFilename { get; private set; }

        #endregion Properties

        #region Constructor

        /// <summary>
        /// Initializes a new instance of the AquaHubLauncher.Model.LauncherUpdateException class.
        /// </summary>
        /// <param name="pathAndFilename">The fileinfo that cause the exception</param>
        public ProcessException(string pathAndFilename)
        {
            this.PathAndFilename = pathAndFilename;
        }

        /// <summary>
        /// Initializes a new instance of the AquaHubLauncher.Model.LauncherUpdateException class
        /// with a specified error message.
        /// </summary>
        /// <param name="pathAndFilename">The fileinfo that cause the exception</param>
        /// <param name="message">The message that describes the error.</param>
        public ProcessException(string pathAndFilename, string message) : base(message)
        {
            this.PathAndFilename = pathAndFilename;
        }

        /// <summary>
        /// Initializes a new instance of the AquaHubLauncher.Model.LauncherUpdateException class with a
        /// specified error message and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="pathAndFilename">The fileinfo that cause the exception</param>
        /// <param name="message">The message that describes the error.</param>
        /// <param name="innerException">The exception that is the cause of the current exception,
        /// or a null reference (Nothing in Visual Basic) if no inner exception is specified.</param>
        public ProcessException(string pathAndFilename, string message, Exception innerException) : base(message, innerException)
        {
            this.PathAndFilename = pathAndFilename;
        }

        #endregion Constructor
    }
}