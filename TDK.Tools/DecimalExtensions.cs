﻿namespace TDK.Tools
{
    public static class DecimalExtensions
    {
        #region Public mehtods

        public static string ToStringDotNotation(this decimal value)
        {
            return value.ToString("0.##").Replace(',', '.');
        }

        #endregion Public mehtods
    }
}