﻿using System;
using System.Data;
using System.Linq;

namespace TDK.Tools
{
    public static class IDataReaderExtension
    {
        #region Public Methods

        public static bool ColumnExists(this IDataReader reader, string columnName)
        {
            return reader.GetSchemaTable()
                         .Rows
                         .OfType<DataRow>()
                         .Any(row => row["ColumnName"].ToString() == columnName);
        }

        #endregion Public Methods

        #region Bool
        public static bool GetBoolSafe(this IDataReader reader, int colIndex)
        {
            return GetBoolSafe(reader, colIndex, false);
        }

        public static bool GetBoolSafe(this IDataReader reader, int colIndex, bool defaultValue)
        {
            return !reader.IsDBNull(colIndex) ? reader.GetBoolean(colIndex) : defaultValue;
        }

        public static bool GetBoolSafe(this IDataReader reader, string indexName)
        {
            if (reader.ColumnExists(indexName))
                return GetBoolSafe(reader, reader.GetOrdinal(indexName));
            else
                return false;
        }

        public static bool GetBoolSafe(this IDataReader reader, string indexName, bool defaultValue)
        {
            return GetBoolSafe(reader, reader.GetOrdinal(indexName), defaultValue);
        }

        public static bool GetIntAsBoolSafe(this IDataReader reader, int colIndex)
        {
            return GetIntAsBoolSafe(reader, colIndex, false);
        }

        public static bool GetIntAsBoolSafe(this IDataReader reader, int colIndex, bool defaultValue)
        {
            int fieldValue = 0;
            if (reader.IsDBNull(colIndex))
                return defaultValue;
            else
            {
                var o = reader.GetValue(colIndex);
                fieldValue = Convert.ToInt32(o);
                return fieldValue == 1;
            }
        }

        public static bool GetIntAsBoolSafe(this IDataReader reader, string indexName)
        {
            if (reader.ColumnExists(indexName))
                return GetIntAsBoolSafe(reader, reader.GetOrdinal(indexName));
            else
                return false;
        }

        public static bool GetIntAsBoolSafe(this IDataReader reader, string indexName, bool defaultValue)
        {
            return GetIntAsBoolSafe(reader, reader.GetOrdinal(indexName), defaultValue);
        }
        #endregion

        #region Decimals

        public static decimal GetDecimalSafe(this IDataReader reader, int colIndex)
        {
            return GetDecimalSafe(reader, colIndex, 0.0M);
        }

        public static decimal GetDecimalSafe(this IDataReader reader, int colIndex, decimal defaultValue)
        {
            return !reader.IsDBNull(colIndex) ? reader.GetDecimal(colIndex) : defaultValue;
        }

        public static decimal GetDecimalSafe(this IDataReader reader, string indexName)
        {
            if (reader.ColumnExists(indexName))
                return GetDecimalSafe(reader, reader.GetOrdinal(indexName));
            else
                return 0.0M;
        }

        public static decimal GetDecimalSafe(this IDataReader reader, string indexName, decimal defaultValue)
        {
            return GetDecimalSafe(reader, reader.GetOrdinal(indexName), defaultValue);
        }

        #endregion Decimals

        #region Strings

        public static string GetStringSafe(this IDataReader reader, int colIndex)
        {
            return GetStringSafe(reader, colIndex, string.Empty);
        }

        public static string GetStringSafe(this IDataReader reader, int colIndex, string defaultValue)
        {
            return !reader.IsDBNull(colIndex) ? reader.GetString(colIndex) : defaultValue;
        }

        public static string GetStringSafe(this IDataReader reader, string indexName)
        {
            if (reader.ColumnExists(indexName))
                return GetStringSafe(reader, reader.GetOrdinal(indexName));
            else
                return string.Empty;
        }

        public static string GetStringSafe(this IDataReader reader, string indexName, string defaultValue)
        {
            return GetStringSafe(reader, reader.GetOrdinal(indexName), defaultValue);
        }

        #endregion Strings

        #region DateTime

        public static DateTime GetDateTimeSafe(this IDataReader reader, int colIndex)
        {
            return GetDateTimeSafe(reader, colIndex, DateTime.MinValue);
        }

        public static DateTime GetDateTimeSafe(this IDataReader reader, int colIndex, DateTime defaultValue)
        {
            return !reader.IsDBNull(colIndex) ? reader.GetDateTime(colIndex) : defaultValue;
        }

        public static DateTime GetDateTimeSafe(this IDataReader reader, string indexName)
        {
            if (reader.ColumnExists(indexName))
                return GetDateTimeSafe(reader, reader.GetOrdinal(indexName));
            else
                return DateTime.MinValue;
        }

        public static DateTime GetDateTimeSafe(this IDataReader reader, string indexName, DateTime defaultValue)
        {
            return GetDateTimeSafe(reader, reader.GetOrdinal(indexName), defaultValue);
        }

        #endregion DateTime

        #region Long

        public static long GetLongSafe(this IDataReader reader, string indexName)
        {
            if (reader.ColumnExists(indexName))
            {
                var colIndex = reader.GetOrdinal(indexName);
                return reader.IsDBNull(colIndex) ? 0 : reader.GetInt64(colIndex);
            }
            else
            {
                return 0;
            }
        }

        #endregion Long

        #region Int

        public static Int32 GetIntSafe(this IDataReader reader, int colIndex)
        {
            return GetIntSafe(reader, colIndex, 0);
        }

        public static Int32 GetIntSafe(this IDataReader reader, int colIndex, Int32 defaultValue)
        {
            return !reader.IsDBNull(colIndex) ? reader.GetInt32(colIndex) : defaultValue;
        }

        public static Int32 GetIntSafe(this IDataReader reader, string indexName)
        {
            if (reader.ColumnExists(indexName))
                return GetIntSafe(reader, reader.GetOrdinal(indexName));
            else
                return 0;
        }

        public static Int32 GetIntSafe(this IDataReader reader, string indexName, Int32 defaultValue)
        {
            return GetIntSafe(reader, reader.GetOrdinal(indexName), defaultValue);
        }

        #endregion Int
    }
}